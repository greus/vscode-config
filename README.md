# Visual Studio Code Config

## Setup

https://code.visualstudio.com/docs/setup/mac#_launching-from-the-command-line

## Extensions

**IntelliJ IDEA Keybindings**

https://marketplace.visualstudio.com/items?itemName=k--kato.intellij-idea-keybindings

**GitLens — Git supercharged**

https://marketplace.visualstudio.com/items?itemName=eamodio.gitlens

**SVG Viewer**

https://marketplace.visualstudio.com/items?itemName=cssho.vscode-svgviewer

**Better TOML**

https://marketplace.visualstudio.com/items?itemName=bungcip.better-toml

### Dart Code (w/ Flutter)
https://marketplace.visualstudio.com/items?itemName=Dart-Code.dart-code

### Python
https://marketplace.visualstudio.com/items?itemName=ms-python.python

**Django Template**

https://marketplace.visualstudio.com/items?itemName=bibhasdn.django-html

**Django pylint args**

```
"python.linting.pylintArgs": [
    "--load-plugins=pylint_django"
],
```

### PHP
https://marketplace.visualstudio.com/items?itemName=felixfbecker.php-intellisense

**Wordpress Snippet**

https://marketplace.visualstudio.com/items?itemName=tungvn.wordpress-snippet

### Vue
https://marketplace.visualstudio.com/items?itemName=octref.vetur
